***This mod is now being migrated into the NodeCore game.***

If you have been using this mod, you should migrate the data into the now-official standard mod before removing it.

**Automatic Migration:**

- Leave this mod installed and enabled.
- Watch the logs on startup after updating the NodeCore game.
- A message like `nc_yctiwy data successfully imported into standard replacement` should appear *exactly once* in your server logs, only on the very first time you start the server with the updated version with the in-built replacement.
- If you miss that message, but see `nc_yctiwy is obsolete and should be removed` on each startup, AND all offline player markers and inventory appear to be correct, then this also indicates a successful import.
- Once you see this message, then you can safely remove this mod, and optionally clean out its mod storage.

**Manual Migration:**

- You can simply rename the `nc_yctiwy` mod storage data to `nc_player_yctiwy` at the same time that you upgrade the base game.
- Make sure you are updating from a version of NodeCore without the nc_player_yctiwy mod to one that does to avoid issues.
- If you are using the old "files" mod storage backend, you can just rename the file.  It may be a bit more involved with the new sqlite backend.

---

When players log out of the server, their items are left behind as non-solid floating entities, with the owner's name attached.  Other players can punch the floating items repeatedly (or "dig" them) to knock them loose and grab them.  Players logging back in will have the same items they had when logging out, minus anything removed by other players.

The intended use is for cooperative games, to prevent a player who has to leave suddenly from leaving with a bunch of supplies needed by the rest of the team. There may also be PvP applications (e.g. to require players to put more thought into theft prevention), but these have not been explored extensively.

You cannot insert items into an offline player's inventory, including returning items you borrowed.  Items need to be left somewhere the offline player will find them when next logging in, and that player will need to voluntarily pick them up.

When using a protection mod, items cannot be stolen from a player who is standing inside their own protected area, unless that area is also accessible by the stealing player.
